package com.sevenheaven.multithreadingloader.concurrent.threads;

import android.util.Log;

import com.sevenheaven.multithreadingloader.ContentDescription;
import com.sevenheaven.multithreadingloader.ContentLoadingListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 7heaven on 16/8/2.
 */
public abstract class DownloadThread extends LoadingThread {

    String mDownloadUrl;

    HttpURLConnection mUrlConnection;

    private static final int MAX_REDIRECTION = 5;

    private List<String> mRequestProperty = new ArrayList<>();

    public DownloadThread(String downloadUrl, String name, ContentLoadingListener loadingListener){
        super(new ContentDescription(name, downloadUrl), loadingListener);

        mDownloadUrl = downloadUrl;
    }

    public void addRequestProperty(String key, String value){
        mRequestProperty.add(key);
        mRequestProperty.add(value);
    }

    @Override
    protected InputStream createInputStream(){
        int redirection = 0;
        String url = mContentDescription.getContentUri();
        try{
            while(redirection < MAX_REDIRECTION){
                HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
                urlConnection.setInstanceFollowRedirects(false);
                urlConnection.setReadTimeout(30000);
                urlConnection.setConnectTimeout(30000);
                if(mContentDescription.currentOffset() > 0){
                    urlConnection.addRequestProperty("Range", "bytes=" + mContentDescription.currentOffset() + "-");
                }

                if(mRequestProperty.size() > 0 && (mRequestProperty.size() & 1) == 0){
                    for(int i = 0; i < mRequestProperty.size(); i += 2){
                        String key = mRequestProperty.get(i);
                        String value = mRequestProperty.get(i + 1);

                        urlConnection.addRequestProperty(key, value);
                    }
                }

                mUrlConnection = urlConnection;

                final int code = urlConnection.getResponseCode();
                switch(code){
                    case HttpURLConnection.HTTP_OK:
                    case HttpURLConnection.HTTP_PARTIAL:
                        mContentDescription.setTotalLength(urlConnection.getContentLength());

                        Log.d("length-got", "length:" + urlConnection.getContentLength());

                        return urlConnection.getInputStream();
                    case HttpURLConnection.HTTP_MOVED_PERM:
                    case HttpURLConnection.HTTP_MOVED_TEMP:
                    case HttpURLConnection.HTTP_SEE_OTHER:
                    case HttpURLConnection.HTTP_NOT_MODIFIED:
                        redirection++;
                        url = urlConnection.getHeaderField("location");
                        Log.d("redirection", url);
                        urlConnection.disconnect();
                        continue;
                    default:
                        Log.d("code", ":" + code);
                        throw new ConnectException("code:" + code);
                }
            }


        }catch(IOException e){
            e.printStackTrace();
            internalSendToHandler(DispatchMsg.ON_FAILURE, e);
        }

        return null;
    }

    @Override
    protected void onTransferProcessEnded(){
        mUrlConnection.disconnect();
    }
}
