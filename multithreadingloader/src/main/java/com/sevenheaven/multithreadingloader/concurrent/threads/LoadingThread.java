package com.sevenheaven.multithreadingloader.concurrent.threads;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.IntDef;
import android.support.annotation.WorkerThread;

import com.sevenheaven.multithreadingloader.ContentDescription;
import com.sevenheaven.multithreadingloader.ContentLoadingListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * abstract class for loading from provided input stream to output stream
 *
 * Created by 7heaven on 16/8/2.
 */
public abstract class LoadingThread implements Runnable{

    /**
     * default max buffer size for input stream
     */
    private static final int DEFAULT_MAX_BUFFER_SIZE = 4096;

    private int mMaxBufferSize = DEFAULT_MAX_BUFFER_SIZE;

    /**
     * end of file flag
     */
    private static final int EOF = -1;

    /**
     * indicate if the loading process is cancelled
     */
    private boolean mCancelled = false;
    private boolean mIsStop = true;

    final ContentLoadingListener mContentLoadingListener;
    ContentDescription mContentDescription;

    private InternalCallback mInternalCallback;

    private String mStoredKey;

    /**
     * easy flag for detect if should invoke listener
     */
    private boolean isLoadingListenerNotNull = false;
    private boolean isInternalCallbackNotNull = false;

    @IntDef({DispatchMsg.ON_START, DispatchMsg.ON_PROGRESS, DispatchMsg.ON_CONTINUE, DispatchMsg.ON_STOP, DispatchMsg.ON_FAILURE, DispatchMsg.THREAD_START, DispatchMsg.THREAD_STOP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DispatchMsg{
        int ON_START = 0;
        int ON_PROGRESS = 1;
        int ON_CONTINUE = 2;
        int ON_STOP = 3;
        int ON_FAILURE = -1;
        int THREAD_START = -2;
        int THREAD_STOP = -3;
    }

    private Handler mExternalHandler;

    private Handler mDispatchHandler = new Handler(){

        @Override
        public void handleMessage(Message message){
            final int msgId = message.what;
            switch(msgId){
                case DispatchMsg.ON_START:
                    dispatchStart();
                    break;
                case DispatchMsg.ON_CONTINUE:
                    dispatchContinue();
                    break;
                case DispatchMsg.ON_PROGRESS:
                    dispatchProgress((float) message.obj);
                    break;
                case DispatchMsg.ON_STOP:
                    dispatchStop((long) message.obj);
                    break;
                case DispatchMsg.ON_FAILURE:
                    dispatchFailure((Throwable) message.obj);
                    break;
                case DispatchMsg.THREAD_START:
                    if(isInternalCallbackNotNull) mInternalCallback.onLoadingThreadStart();
                    break;
                case DispatchMsg.THREAD_STOP:
                    if(isInternalCallbackNotNull) mInternalCallback.onLoadingThreadStop();
                    break;
            }
        }
    };

    public interface InternalCallback{
        void onLoadingThreadStart();
        void onLoadingThreadStop();
    }

    public LoadingThread(ContentDescription contentDescription, ContentLoadingListener contentLoadingListener){
        if(contentDescription == null) throw new IllegalArgumentException("contentDescription must not be null!");

        mContentLoadingListener = contentLoadingListener;
        mContentDescription = contentDescription;

        isLoadingListenerNotNull = mContentLoadingListener != null;
    }

    public void setInternalCallback(InternalCallback callback){
        mInternalCallback = callback;
        if(mInternalCallback != null){
            isInternalCallbackNotNull = true;
        }else{
            isInternalCallbackNotNull = false;
        }
    }

    public InternalCallback getInternalCallback(){
        return mInternalCallback;
    }

    public void setKey(String key){
        mStoredKey = key;
    }

    public String getKey(){
        return mStoredKey;
    }

    @Override
    @WorkerThread
    final public void run(){
        internalSendToHandler(DispatchMsg.THREAD_START);

        mIsStop = false;
        InputStream is = createInputStream();
        OutputStream ops = createOutputStream();

        long count = -1;

        if(is != null && ops != null) {
            try {
                count = transfer(is, ops);
                mContentDescription = new ContentDescription(mContentDescription.getContentName(),
                        mContentDescription.getContentUri(),
                        mContentDescription.getTotalLength(),
                        count + mContentDescription.currentOffset());
                ops.flush();
            } catch (IOException e) {
                e.printStackTrace();
                if(!mCancelled) internalSendToHandler(DispatchMsg.ON_FAILURE, e);
            } finally {
                try {
                    is.close();
                    ops.close();
                    if(!mCancelled) internalSendToHandler(DispatchMsg.ON_STOP, count);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                onTransferProcessEnded();
            }
        }else{
            if(!mCancelled) internalSendToHandler(DispatchMsg.ON_FAILURE, new NullPointerException());
        }
        mIsStop = true;

        internalSendToHandler(DispatchMsg.THREAD_STOP);
    }

    /**
     * main transfer process, transfer bytes data from is to ops
     * @param is input stream
     * @param ops output stream
     * @throws IOException
     */
    @WorkerThread
    private long transfer(InputStream is, OutputStream ops) throws IOException {

        if(mContentDescription.isResumeFromPreviousLoading()){
            internalSendToHandler(DispatchMsg.ON_CONTINUE);
        }else{
            internalSendToHandler(DispatchMsg.ON_START);
        }

        /**
         *
         * TODO make this more elegant
         * try set total length to ContentDescription if total length in ContentDescription is 0
         * there's no guarantee that InputStream.available() will return total length of the content
         */
        long totalLength = is.available();
        mContentDescription.setTotalLength(totalLength);
        if(totalLength <= 0) totalLength = mContentDescription.getTotalLength();

        final boolean totalLengthGot = totalLength > 0;

        byte[] buffers = new byte[mMaxBufferSize];
        int length = 0;
        long count = 0;
        while(!mCancelled && (length = is.read(buffers)) > 0){
            ops.write(buffers, 0, length);

            count += length;

            if(totalLengthGot) internalSendToHandler(DispatchMsg.ON_PROGRESS, (float) count / (float) totalLength);
        }

        return count;
    }

    /**
     * create a OutputStream for loading process output
     * @return
     */
    @WorkerThread
    protected abstract OutputStream createOutputStream();

    /**
     * create a InputStream as loading process input
     * @return
     */
    @WorkerThread
    protected abstract InputStream createInputStream();

    protected void onTransferProcessEnded(){

    }


    /**
     * dispatch start callback to ContentLoadingListener, child class may override this method to make different callback strategy
     */
    protected void dispatchStart(){
        if(isLoadingListenerNotNull){
            mContentLoadingListener.onStart(mContentDescription);
        }
    }

    /**
     * dispatch progress callback to ContentLoadingListener, child class may override this method to make different callback strategy
     */
    protected void dispatchProgress(float progress){
        if(isLoadingListenerNotNull){
            mContentLoadingListener.onProgress(mContentDescription, progress);
        }
    }

    /**
     * dispatch continue callback to ContentLoadingListener, child class may override this method to make different callback strategy
     */
    protected void dispatchContinue(){
        if(isLoadingListenerNotNull){
            mContentLoadingListener.onContinue(mContentDescription, mContentDescription.currentOffset());
        }
    }

    /**
     * dispatch stop callback to ContentLoadingListener, child class may override this method to make different callback strategy
     */
    protected void dispatchStop(long loadedBytes){
        if(isLoadingListenerNotNull){
            mContentLoadingListener.onStop(mContentDescription, loadedBytes);
        }
    }

    protected void dispatchFailure(Throwable e){
        if(isLoadingListenerNotNull){
            mContentLoadingListener.onFailure(mContentDescription, e);
        }
        cancel();
    }

    @WorkerThread
    protected void internalSendToHandler(@DispatchMsg int dispatchMsgId){
        internalSendToHandler(dispatchMsgId, null);
    }

    @WorkerThread
    protected void internalSendToHandler(@DispatchMsg int dispatchMsgId, Object extObj){
        Message message = Message.obtain(mDispatchHandler, dispatchMsgId, extObj);
        message.sendToTarget();

        handleGlobalNotification(message);
    }

    public ContentDescription getContentDescription(){
        return mContentDescription;
    }

    public final void setExternalHandler(Handler handler){
        mExternalHandler = handler;
    }

    private void handleGlobalNotification(Message msg){
        if(mExternalHandler != null){
            Message externalMessage = Message.obtain(msg);
            mExternalHandler.sendMessage(externalMessage);
        }
    }

    final public void cancel(){
        mCancelled = true;
    }

    final public boolean isCancelled(){
        return mCancelled;
    }

    final public boolean isStop(){
        return mIsStop;
    }
}
