package com.sevenheaven.multithreadingloader.concurrent.threads;

import com.sevenheaven.multithreadingloader.ContentDescription;
import com.sevenheaven.multithreadingloader.ContentLoadingListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by 7heaven on 16/8/23.
 */
public class FilesTransferThread extends LoadingThread {

    private File mInputFile;
    private File mOutputFile;
    private boolean mOverwrite;

    public FilesTransferThread(File inputFile, File outputFile, ContentLoadingListener contentLoadingListener){
        this(inputFile, outputFile, contentLoadingListener, false);
    }

    public FilesTransferThread(File inputFile, File outputFile, ContentLoadingListener contentLoadingListener, boolean overwrite){
        super(inputFile == null || outputFile == null ? null : new ContentDescription(inputFile.getName(), inputFile.getAbsolutePath(), inputFile.length(), 0), contentLoadingListener);

        mInputFile = inputFile;
        mOutputFile = outputFile;

        mOverwrite = overwrite;
    }

    @Override
    protected InputStream createInputStream(){
        try{
            FileInputStream fis = new FileInputStream(mInputFile);

            return fis;
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected OutputStream createOutputStream(){
        try{
            boolean preLoad = false;
            if(mOverwrite) mOutputFile.deleteOnExit();
            if(!mOutputFile.exists()){
                preLoad = mOutputFile.mkdirs() && mOutputFile.createNewFile();
            }

            if(preLoad){
                FileOutputStream fos = new FileOutputStream(mOutputFile);

                return fos;
            }else{

            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        return null;
    }
}
