package com.sevenheaven.multithreadingloader.concurrent.threads;

import com.sevenheaven.multithreadingloader.ContentDescription;
import com.sevenheaven.multithreadingloader.ContentLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by 7heaven on 16/8/2.
 */
public class DownloadToFileThread extends DownloadThread {

    File mOutputFile;

    private FileOutputStream mCachedOutputStream;

    public DownloadToFileThread(String downloadUrl, File outputFile, ContentLoadingListener listener){
        super(downloadUrl, outputFile.getName(), listener);

        mOutputFile = outputFile;

        if(mOutputFile.exists()){
            mContentDescription = new ContentDescription(mContentDescription.getContentName(), mContentDescription.getContentUri(), mContentDescription.getTotalLength(), mOutputFile.length());
        }
    }

    @Override
    protected OutputStream createOutputStream(){
        if(mOutputFile != null){
            try{

                //TODO unnecessary codes?
                if(mCachedOutputStream != null){
                    mCachedOutputStream.flush();
                    mCachedOutputStream.close();
                }
                if(mContentDescription.currentOffset() > 0){
                    if(mOutputFile.length() == mContentDescription.currentOffset()){
                        mCachedOutputStream = new FileOutputStream(mOutputFile, true);
                    }
                }else{
                    if(mOutputFile.exists()) mOutputFile.delete();
                    mCachedOutputStream = new FileOutputStream(mOutputFile);
                }

                return mCachedOutputStream;
            }catch(Exception e){
                internalSendToHandler(DispatchMsg.ON_FAILURE, e);
            }
        }

        internalSendToHandler(DispatchMsg.ON_FAILURE, new IOException());

        return null;
    }

    //TODO check if necessary
    @Override
    protected void onTransferProcessEnded(){
        if(mCachedOutputStream != null){
            try{
                mCachedOutputStream.flush();
                mCachedOutputStream.close();
            }catch(IOException e){
                e.printStackTrace();
            }finally {
                mCachedOutputStream = null;
            }

        }
    }
}
