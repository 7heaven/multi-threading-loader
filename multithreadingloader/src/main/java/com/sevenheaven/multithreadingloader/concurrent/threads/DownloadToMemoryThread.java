package com.sevenheaven.multithreadingloader.concurrent.threads;

import com.sevenheaven.multithreadingloader.ContentDescription;
import com.sevenheaven.multithreadingloader.ContentLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Created by 7heaven on 16/8/2.
 */
public class DownloadToMemoryThread extends DownloadThread {

    private ByteArrayOutputStream mOutputStream;

    private boolean mIsTransferEnded = false;

    public DownloadToMemoryThread(String downloadUrl, String name, ContentLoadingListener listener){
        super(downloadUrl, name, listener);
    }

    @Override
    protected OutputStream createOutputStream(){
        mOutputStream = new ByteArrayOutputStream();

        return mOutputStream;
    }

    @Override
    protected void onTransferProcessEnded(){
        mIsTransferEnded = true;

        // Download to memory does not support resume from break-point,
        // so we set ContentDescription offset to zero in-case this is a pause action
        mContentDescription = new ContentDescription(mContentDescription.getContentName(),
                mContentDescription.getContentUri(),
                mContentDescription.getTotalLength(),
                0);
    }

    public byte[] getByteArray(){
        if(mOutputStream != null){
            return mOutputStream.toByteArray();
        }
        return null;
    }
}
