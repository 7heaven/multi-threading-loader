package com.sevenheaven.multithreadingloader;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.IntDef;

import com.sevenheaven.multithreadingloader.concurrent.threads.LoadingThread;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 7heaven on 16/8/2.
 */
public class ContentLoadingManager {

    private static ContentLoadingManager sInstance;

    private ExecutorService mExecutorService;

    private Map<String, LoadingThread> mCachedTasks;
    private Map<String, LoadingThread> mPausedTasks;
    private Map<String, ObserverHandler> mCachedObserverHandlers;
    private Map<String, ObserverHandler> mPausedObserverHandlers;

    @IntDef({Result.OK, Result.EXISTS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Result{
        int OK = 0;
        int EXISTS = 1;
    }

    private static class ObserverHandler extends Handler {

        private List<WeakReference<ContentLoadingListener>> mObserversRef = new ArrayList<>();

        private String mTaskKey;
        private ContentDescription mContentDescription;

        public ObserverHandler(String taskKey, ContentDescription contentDescription){
            mTaskKey = taskKey;
            mContentDescription = contentDescription;
        }

        interface ObserverAccessor{
            void access(ContentLoadingListener listener);
        }

        public void addObserver(ContentLoadingListener listener){
            mObserversRef.add(new WeakReference<>(listener));
        }

        @Override
        public void handleMessage(final Message msg){
            accessObservers(new ObserverAccessor() {
                @Override
                public void access(ContentLoadingListener listener) {
                    final int msgId = msg.what;
                    switch(msgId){
                        case LoadingThread.DispatchMsg.ON_START:
                            listener.onStart(mContentDescription);
                            break;
                        case LoadingThread.DispatchMsg.ON_CONTINUE:
                            listener.onContinue(mContentDescription, mContentDescription.currentOffset());
                            break;
                        case LoadingThread.DispatchMsg.ON_PROGRESS:
                            listener.onProgress(mContentDescription, (float) msg.obj);
                            break;
                        case LoadingThread.DispatchMsg.ON_STOP:
                            listener.onStop(mContentDescription, (long) msg.obj);
                            break;
                        case LoadingThread.DispatchMsg.ON_FAILURE:
                            listener.onFailure(mContentDescription, (Throwable) msg.obj);
                            break;
                        case LoadingThread.DispatchMsg.THREAD_START:
                            break;
                        case LoadingThread.DispatchMsg.THREAD_STOP:
                            ContentLoadingManager.getInstance().removeTaskAndHandler(mTaskKey);
                            break;
                    }
                }
            });
        }

        private void accessObservers(ObserverAccessor accessor){
            if(accessor != null){
                final int size = mObserversRef.size();
                for(int i = 0; i < size; i++){
                    ContentLoadingListener listener = mObserversRef.get(i).get();
                    if(listener != null){
                        synchronized (listener){
                            accessor.access(listener);
                        }
                    }
                }
            }
        }
    }

    public static ContentLoadingManager getInstance(){
        synchronized (ContentLoadingManager.class){
            if(sInstance == null){
                sInstance = new ContentLoadingManager();
            }
        }

        return sInstance;
    }

    private ContentLoadingManager(){
        mExecutorService = Executors.newFixedThreadPool(3);
        mCachedTasks = new ConcurrentHashMap<>();
        mPausedTasks = new ConcurrentHashMap<>();
        mCachedObserverHandlers = new ConcurrentHashMap<>();
        mPausedObserverHandlers = new ConcurrentHashMap<>();
    }

    public boolean checkTaskExist(String key){
        return mCachedTasks.containsKey(key);
    }

    public int startTask(final String key, final LoadingThread thread){
        if(mCachedTasks.containsKey(key)){
            if(mCachedTasks.get(key).isStop()){
                mCachedTasks.remove(key);
            }else{
                return Result.EXISTS;
            }

        }

        mCachedTasks.put(key, thread);
        addObserver(key, null);

        mExecutorService.execute(thread);
        return Result.OK;
    }

    public void resumeTask(String taskKey){
        if(mPausedTasks.containsKey(taskKey)){
            LoadingThread task = mPausedTasks.get(taskKey);
            ObserverHandler handler = mPausedObserverHandlers.get(taskKey);

            task.setExternalHandler(handler);
            mExecutorService.execute(task);
        }
    }

    public void pauseTask(String taskKey){
        if(mCachedTasks.containsKey(taskKey)){
            LoadingThread task = mCachedTasks.get(taskKey);
            ObserverHandler handler =mCachedObserverHandlers.get(taskKey);

            cancelTask(taskKey);

            mPausedTasks.put(taskKey, task);
            mPausedObserverHandlers.put(taskKey, handler);
        }
    }

    public boolean addObserver(String taskKey, ContentLoadingListener listener){
        LoadingThread task = mCachedTasks.get(taskKey);
        if(task != null){
            ObserverHandler mTaskHandler = null;
            ContentDescription contentDescription = task.getContentDescription();
            if(mCachedObserverHandlers.containsKey(taskKey)){
                mTaskHandler = mCachedObserverHandlers.get(taskKey);
            }else{
                mTaskHandler = new ObserverHandler(taskKey, contentDescription);
                mCachedObserverHandlers.put(taskKey, mTaskHandler);

                task.setExternalHandler(mTaskHandler);
            }

            mTaskHandler.addObserver(listener);
        }

        return false;
    }

    public void cancelTask(String key){
        if(mCachedTasks.containsKey(key)){
            mCachedTasks.get(key).cancel();
        }
    }

    public boolean setExecutor(ExecutorService executor){

        return false;
    }

    void removeTaskAndHandler(String taskKey){
        mCachedObserverHandlers.remove(taskKey);
        LoadingThread task = mCachedTasks.remove(taskKey);
        if(task != null) task.setExternalHandler(null);
    }

}
