package com.sevenheaven.multithreadingloader;

import java.io.Serializable;

/**
 * Created by 7heaven on 16/8/2.
 */
final public class ContentDescription implements Serializable{

    private long mTotalLength;

    private long mStartFromOffset = 0;

    private String mContentName;
    private String mContentUri;

    public ContentDescription(String name, String uri){
        this(name, uri, 0, 0);
    }

    public ContentDescription(String name, String uri, long totalLength, long startOffset){
        mContentName = name;
        mContentUri = uri;
        mTotalLength = totalLength;
        mStartFromOffset = startOffset;
    }

    public boolean isResumeFromPreviousLoading(){
        return mStartFromOffset > 0;
    }

    public long currentOffset(){
        return mStartFromOffset;
    }

    public long getTotalLength(){
        return mTotalLength;
    }

    public void setTotalLength(long totalLength){
        if(mTotalLength == 0) mTotalLength = totalLength;
    }

    final public String getContentUri(){
        return mContentUri;
    }

    final public String getContentName(){
        return mContentName;
    }
}
