package com.sevenheaven.multithreadingloader;

/**
 * Created by 7heaven on 16/8/2.
 */
public interface ContentLoadingListener {

    /**
     * call for loading progress change, may not call if we does not get the total length of content
     *
     * @param contentDesc contentDescription
     * @param progress loading progress
     */
    void onProgress(ContentDescription contentDesc, float progress);

    /**
     * call when a content loading process start
     *
     * @param contentDesc contentDescription
     */
    void onStart(ContentDescription contentDesc);

    /**
     * call when a previously paused content loading process continue, based on ContentDescription's startOffset
     *
     * @param contentDesc ContentDescription
     * @param startBytes start offset
     */
    void onContinue(ContentDescription contentDesc, long startBytes);

    /**
     * call when content loading stop, whether content loading complete or not
     *
     * @param contentDesc ContentDescription
     * @param loadedBytes loaded bytes count
     */
    void onStop(ContentDescription contentDesc, long loadedBytes);

    void onFailure(ContentDescription contentDesc, Throwable e);
}
