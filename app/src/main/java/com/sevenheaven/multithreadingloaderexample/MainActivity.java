package com.sevenheaven.multithreadingloaderexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sevenheaven.multithreadingloader.ContentLoadingManager;
import com.sevenheaven.multithreadingloader.concurrent.threads.DownloadToFileThread;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ContentLoadingManager.getInstance().startTask("download", new DownloadToFileThread());
    }
}
